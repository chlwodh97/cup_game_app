import 'package:flutter/material.dart';

class CupChoiceItem extends StatefulWidget {
  const CupChoiceItem({
    super.key,
    required this.pandon,
    required this.currentMoney
  });

  final num pandon; // 100
  final num currentMoney; // 0

  @override
  State<CupChoiceItem> createState() => _CupChoiceItemState();
}

class _CupChoiceItemState extends State<CupChoiceItem> {
  bool _isOpen = false;
  String imgSrc = 'assets/cup.jpeg';

  void _calculateStart() {
    if (!_isOpen) {
      setState(() {
        _isOpen = true;
      });
    }
  }

  void _calculateImgSrc() {
    String tempImgSrc = '';
    if (_isOpen) {
      if (widget.pandon < widget.currentMoney) {
        tempImgSrc = 'assets/0one.jpeg';
      } else if (widget.pandon == widget.currentMoney) {
        tempImgSrc = 'assets/100one.jpeg';
      } else {
        tempImgSrc = 'assets/200one.png';
      }
    } else {
      tempImgSrc = 'assets/cup.jpeg';
    }

    setState(() {
      imgSrc = tempImgSrc;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _calculateStart();
        _calculateImgSrc();
      },
      child: Image.asset(imgSrc,
      width: 100,
      height: 100,),
    );
  }
}
